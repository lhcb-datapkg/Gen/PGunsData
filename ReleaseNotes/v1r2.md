2015-03-19 PGunsData v1r2
===

**[2015-03-13] Included input spectrum for B+ and Bs0 decays (pt vs eta and
 pt vs pz) in Ebeam4000GeV**   
by Stefanie Reichert   
This histograms should be replace by histograms with full statistics after
initial test of PGun productions

